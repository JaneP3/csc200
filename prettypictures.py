
from gasp import *

begin_graphics()

def draw_face(x, y):
    Circle((x, y), 40)
    Circle((x-15, y+10), 5)
    Circle((x+15, y+10), 5)
    Line((x, y+10), (x-10, y-10))
    Line((x-10, y-10), (x+10, y-10))
    Arc((x, y-17), 20, 180, 180)
    Line((x+20, y-17), (x-20, y-17))

def draw_body(x, y):
    Line((x, y-40), (x, y-120))
    Line((x, y-60), (x-60, y-40))
    Line((x, y-60), (x+60, y-40))
    Line((x, y-120), (x-50, y-160))
    Line((x, y-120), (x+50, y-160))

def draw_ball(x, y):
    Circle((x-5, y), 10)


draw_face(300, 200)
draw_body(300, 200)
draw_ball(240, 160)

update_when('key_pressed')
end_graphics()
