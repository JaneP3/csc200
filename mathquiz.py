import random

print("Welcome To The Math Quiz")
numquest = int(input("How many questions do you want?: "))
correct_count = 0
questions = numquest
while numquest > 0:
    num1 = random.randint(1,50)
    num2 = random.randint(1,50)
    print("What is ", num1, " multiplied by ", num2, "?")
    answer = int(input())
    if answer == num1 * num2:
        print("That’s right – well done")
        correct_count += 1
    else:
        print("No, I'm afraid the answer was ", num1*num2)
    print("--------------")
    numquest -= 1

print("I asked you ", questions, " questions. You got ", correct_count, " of them right. Well done!")

