import xml.etree.ElementTree as ET

data = '''
<stuff>    
    <contacts>
        <person>
            <name>Jane</name>
            <phone>703 554 6715</phone>
        </person>
        <person>
            <name>Phil</name>
            <phone>703 675 3399</phone>
        </person>
    </contacts>
</stuff>'''

stuff = ET.fromstring(data)
lst = stuff.findall('contacts/person')
print('Contact count:', len(lst))

for item in lst:
    print('Name:', item.find('name').text)
    print('Phone:', item.find('phone').text)
