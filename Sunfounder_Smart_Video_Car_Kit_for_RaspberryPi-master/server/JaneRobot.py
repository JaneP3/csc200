#!/usr/bin/env python
import RPi.GPIO as GPIO
import video_dir
import car_dir
import motor
from socket import *
from time import ctime          # Import necessary modules   
import time

ctrl_cmd = ['forward', 'backward', 'left', 'right', 'stop', 'read cpu_temp', 'home', 'distance', 'x+', 'x-', 'y+', 'y-', 'xy_home']

busnum = 1          # Edit busnum to 0, if you uses Raspberry Pi 1 or 0

HOST = ''           # The variable of HOST is null, so the function bind( ) can be bound to all valid addresses.
PORT = 21567
BUFSIZ = 1024       # Size of the buffer
ADDR = (HOST, PORT)

tcpSerSock = socket(AF_INET, SOCK_STREAM)    # Create a socket.
tcpSerSock.bind(ADDR)    # Bind the IP address and port number of the server. 
tcpSerSock.listen(5)     # The parameter of listen() defines the number of connections permitted at one time. Once the 
                         # connections are full, others will be rejected. 

video_dir.setup(busnum=busnum)
car_dir.setup(busnum=busnum)
motor.setup(busnum=busnum)     # Initialize the Raspberry Pi GPIO connected to the DC motor. 
video_dir.home_x_y()
car_dir.home()

print 'Wait 10 seconds'
time.sleep(10)
print 'Starting sequence'
time.sleep(1)
print 'motor moving forward'
motor.forward()
time.sleep(3)
print 'move -x'
video_dir.move_decrease_x()
time.sleep(2)
print 'move +x'
video_dir.move_increase_x()
time.sleep(2)
print 'recv backward cmd'
motor.backward()
time.sleep(3)
print 'move -x'
video_dir.move_decrease_x()
time.sleep(2)
print 'move +x'
video_dir.move_increase_x()
time.sleep(2)





