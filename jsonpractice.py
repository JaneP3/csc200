import json

input = '''
[
    { "name" : "Jane",
      "id" : "001",
      "dogs" : "yes"
    } ,
    { "name" : "Michael",
      "id" : "005",
      "dogs" : "no"
    } ,
    { "name" : "Alison",
      "id" : "008",
      "dogs" : "no"
    } ,
    { "name" : "Chris",
      "id" : "012",
      "dogs" : "yes"
    }
]'''

info = json.loads(input)
print('User count:', len(info))

for item in info:
    print('----------------------------')
    print('Name:', item["name"])
    print('Id:', item["id"])
    print('Dogs:', item["dogs"])
